# include <stdio.h>

int main(){
	
	float percent;
	char flag;	
	start: printf("\nPlease enter the percent to know the grade");
	
	scanf("%f",&percent);
	
	if (percent <0 || percent > 100){
		
		printf("\nInvalid Number please enter valid number");
		goto start;
	}
	
	if (percent >= 75){
		printf("\nGrade A+");	
	}
	
	if (percent > 60 && percent <75) {
		printf("\nGrade A");
		//goto lbl;
	} 
	
	if (percent > 50 && percent<40){
		printf("\nGrade B");
	}
	
	if (percent >= 35 && percent<39){
		printf("\nPass Class");
	} 
	
	if (percent < 35)  {
		printf("\nFail");
	}
	
	lbl: printf("\nThanks for using software");
	
	flaglbl:printf("\nPress Y to continue N to exit");
	
	scanf(" %c",&flag);
	
	if(flag != 'y' && flag != 'Y' && flag != 'n' && flag != 'N') {
		printf("\nInvalid input char");
		goto flaglbl;
	}
	
	if (flag == 'Y' || flag== 'y'){
		
		goto start;
	}
	
	if (flag == 'N' || flag == 'n'){
		
		printf("Good Bye");
	}
	
	
}
