#include <stdio.h>



int main(){
	
/*variable declaration*/	
	
	int r;
	int diameter;
	float pie;
	int areaofcircle;
	int circumference;
	
	/*Input value for radius as per user requirement */
	
	printf("Enter the radius of the circle:");
	scanf("%d", &r);
	printf("\nRadius is %d",r);
	
	/* Calculation starts */
	
	pie = 22/7.0;
	diameter = r*2;
	areaofcircle = pie*r*r;
	circumference = 2*pie*r;
	
	/* Calculation ends */
	
	/* Display the output after calculation */
	
	printf("\nPie Value: %.2f",pie);
	printf("\nDiameter: %d",diameter);
	printf("\nArea of Circle: %d",areaofcircle);
	printf("\nCiecumference of Circle: %d",circumference);
	
	/* program ends*/
	
	
	
}
