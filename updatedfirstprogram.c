# include <stdio.h>

//Declare variables
int main()

{
	int english;
	int marathi;
	int hindi;
	int maths;
	int science;
	int history;
	int total;
	float percent;
	
	//Program starts	
	printf("Welcome to result.com\n");
	
	//prompt to enter the student name for whiom result is to be check
	char name[20];
	start:printf("\nPlease enter the student name:");

	scanf(" %[^\n]s",&name);
	printf("\nStudent Name: %s",name);
		
	//User is requested to enter the marks obtained in respective subject
	printf("\nEnter the mark obtained for the below subject");
	
	// entering subject mark starts
	printf("\nEnglish:");
	scanf("%d",&english);
	
	printf("\nMarathi:");
	scanf("%d",&marathi);
	
	printf("\nHindi:");
	scanf("%d",&hindi);
	
	printf("\nMaths:");
	scanf("%d",&maths);
	
	printf("\nScience:");
	scanf("%d",&science);
	
	printf("\nHistory:");
	scanf("%d",&history);
	
	//entering subject marks ends and total and percent calculation starts
 	
	total = english + marathi+hindi+maths+science+history;
	percent= (total/600.0)*100;
	
	// Total and percent calculation is done and grade calculation starts
	
	//if percent is less then o or greater then 100 then is prompt user to check the marks enter. 
	if (percent <0 )
	{
		printf("\nResult:- \n Please check the mark entered");
		goto recheck;
	}
	
	if (percent > 100)
		{
			printf("\nResult:- \n Please check the mark entered");
			goto recheck;
		}

// Normal grade calculation

	if (percent > 75)	
		{
			printf("\nResult:- \n Total: %d Percent: %.2f Grade: A+",total,percent);
			
		}
		
	if (percent <=75 && percent > 60)
		{
			printf("\nResult:- \n Total: %d Percent: %.2f Grade: A",total,percent);
			
		}
	
	if (percent <=60 && percent > 45)
		{
			printf("\nResult:- \n Total: %d Percent: %.2f Grade: B",total,percent);
			
		}
		
	if (percent <=45 && percent >= 35)
		{
			printf("\nResult:- \n Total: %d Percent: %.2f Grade: C",total,percent);
			
		}
	
	if (percent <35 && percent >= 0)
		{
			printf("\nResult:- \n Total: %d Percent: %.2f Grade: Fail",total,percent);
			
		}
		
//it will ask user If he wants to check result for any other student if yes it will proceed or exit
 	
	char flag1;
	restart:printf("\nPlease enter Y for next student or N to exit: ");
	
	scanf(" %c", &flag1);
	
	 	if (flag1 == 'Y' || flag1== 'y'){
 		
 		goto start;
 	}
// if user wants to exit
 
	if (flag1 == 'N' || flag1 == 'n')
	{
		printf("Thank you for using result.com Bye-Bye!");
	}


// It will ask user to recheck marks if abnormal percent is calculated	
	char flag;
	recheck:printf("\nPlease enter Y to recheck or N to exit: ");
	
	scanf(" %c", &flag);
	
	 	if (flag == 'Y' || flag== 'y'){
 		
 		goto start;
 	}

	if (flag == 'N' || flag == 'n')
	{
		printf("Thank you for using result.com Bye-Bye!");
	}
	
	//Program Ends.
	
	
	
	
	
}


