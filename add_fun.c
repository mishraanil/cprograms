#include <stdio.h>
#include "my_math.h"

int main(){
	int x = 5;
	int y = 6;
	int sum;
	sum = add(x, y);
	
	printf("\n%d + %d = %d", x, y, sum);
	
	printf("\nSquare of %d = %d", x, square(x));
	
	printf("\nSquare of %d = %d", y, square(y));
	
	printf("\n%d * %d = %d", x, y, mul(x,y));
	
	getch();
}
