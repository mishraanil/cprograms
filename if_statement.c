/* if else statement */

#include <stdio.h>

int main() {
	float percent;
	
	printf("Enter Percentage ");
	scanf("%f", &percent);
	
	if(percent > 35) {
		printf("\nStudent is passed");
	} else {
		printf("\nStudent is failed");
	}
	
}
