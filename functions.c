#include <stdio.h>
/* defining printStar() function */
void printStar(){ // function heading
	printf("\n*****");// function body or block of code
}

int add(){// function returning int value
	printf("\nStars from add function");
	printStar(); 
	printf("\n");
	printf("Stars from add function Ends\n");
	return 5 + 5;
}

int addWithArguments(int x, int y){
	return x + y;
}

int main(){
	printStar(); // calling a function
	printf("\nHello WOrld");
	printStar();
	int total = add();
	printf("\n%d", total);
	total = 5 + add();
	printf("\n%d", total);
	printf("\n%d", addWithArguments(5,6));
	printf("\n%d", addWithArguments(10,20));
	
	int x = 5;
	int y = 6;
	int z = 7;
	
	printf("\n%d + %d + %d = %d", x, y, z, addWithArguments(addWithArguments(x,y), z));
	
	getch();
}
