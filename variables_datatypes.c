#include <stdio.h> // because we are going to use printf() and scanf() function

int main(){ // entry point of any c program
	int a = 5; // a is a variable of type integer which stores value 5 (4 bytes)
	int b = 7;
	int c = a + b;// adding a+b and stores into variable c
	
	printf("Size of int : %d", sizeof(int)); // sizeof() function to show size of any data type or variable
	//printf("Size of int : %d", sizeof(a));
	
	printf("\n%d + %d = %d", a, b, c); // %d is replaced by value of c
	
	//%d is format specifier
	
	int num1; // declared num1 variable of type int
	int num2;
	
	printf("\nPlease enter number 1: ");
	scanf("%d", &num1);
	
	printf("\nPlease enter number 2: ");
	scanf("%d", &num2);
	
	
	printf("\n%d + %d = %d", num1, num2, (num1 + num2));
	
}
