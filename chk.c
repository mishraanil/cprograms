#include <stdio.h>
 
 int main(){
 	char flag;
 	
 	lbl:printf("\nPlease enter Y to continue or N to Exit:"); // statement with label
 //	scanf("%c",&flag); // read character from input screen
 	scanf(" %c",&flag);
 	printf("\nhere is charachter %c you\'ve entered", flag); // print char on screen
 	
 	if(flag == 'Y' || flag == 'y'){ // check if char is y or Y
 		printf("\nProgram Continued");
 		goto lbl; // if y or Y then go to label lbl
 	}
 	
 	if (flag =='N' || flag =='n'){ // check if char is n or N
 		printf("\nGood Bye"); // if n or N then exit the program
 	}
 	
 	if(flag == '\n'){
 		printf("\nOhh you\'ve pressed enter key");
 	}
 }
