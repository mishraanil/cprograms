<%-- 
    Document   : database
    Created on : Dec 30, 2016, 8:29:21 PM
    Author     : ICIT
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="com.Database"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    Database d = new Database("root", "", "icit");
    
    String order_by = request.getParameter("orderby");
    ResultSet rs = d.selectRecord("SELECT * FROM students ORDER BY " + order_by);

%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
        out.println(order_by);
        %>
        <table border="1">
            <thead>
                <th><a href="http://localhost:8084/AnilWebProject/database.jsp?orderby=name">Name</a></th>
                <th><a href="http://localhost:8084/AnilWebProject/database.jsp?orderby=age">Age</a></th>
                <th><a href="http://localhost:8084/AnilWebProject/database.jsp?orderby=city">City</a></th>
            </thead>
        <% while (rs.next()) { %>
        <tr>
            <td><%=rs.getString("name")%></td>
            <td><%=rs.getString("age")%></td>
            <td><%=rs.getString("city")%></td>
        </tr>
        <% } %>   
        </table>
    </body>
</html>
