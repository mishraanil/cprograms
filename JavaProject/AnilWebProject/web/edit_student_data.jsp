<%-- 
    Document   : edit_student_data
    Created on : Jan 7, 2017, 8:21:05 PM
    Author     : ICIT
--%>

<%@page import="java.sql.ResultSet"%>
<%@page import="com.MysqlCrudDb1"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    MysqlCrudDb1 p = new MysqlCrudDb1("root", "", "icit");
    String student_id = request.getParameter("id");
    
    String sql      = "SELECT * FROM students WHERE id = '" + student_id + "'";
    ResultSet rs    = p.selectRecord(sql);
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Edit Student Data</h1>
        <form action="edit_student_data_action.jsp">
        <table border="1">
                <% while (rs.next()) { %>
                <tr>
                    <td colspan="1">
                        Id
                    </td>
                    <td colspan="1">
                        <input type="text" name="student_id" value="<%=student_id%>">
                    </td>
                </tr>
                <tr>
                    <td colspan="1">
                        Name 
                    </td>
                    <td colspan="1">
                        <input type="text" name="name" value="<%=rs.getString("name")%>">
                    </td>
                </tr>
                <tr>
                    <td colspan="1">
                        Age
                    </td>
                    <td colspan="1">
                        <input type="text" name="age" value="<%=rs.getString("age")%>">
                        
                    </td>
                </tr>
                <tr>
                    <td colspan="1">
                        City
                    </td>
                    <td colspan="1">
                        <input type="text" name="city" value="<%=rs.getString("city")%>">
                    </td>
                </tr>
                <tr>
                    <td colspan="1">
                        Mobile No
                    </td>
                    <td colspan="1">
                        <input type="text" name="mobile_number" value="<%=rs.getString("mobile_number")%>">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <input type="submit" value="update">
                    </td>
                </tr>


                <% }%>            
            </table>
        </form>
    </body>
</html>
