#include <stdio.h>

int main(){
	int arr[3][4] = {
					{1, 2, 3, 44}, 
					{4, 5, 6},
					{7, 8, 9}
					};
	//[2] : number of rows
	//[3] : columns in a row
	
	/* accessing multidimensional array */
	//arr[row number][column number]
	printf("%d\n", arr[0][0]);
	
	
	int i,j;
	
	for(i = 0; i <= 2; i++) { // for row count
		for(j = 0; j <= 3; j++) { // for column count
			printf("%d ", arr[i][j]);		
		}
		printf("\n");
	}
	
	
	getch();
	
}
