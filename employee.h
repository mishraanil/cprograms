struct DateOfBirth{
	int d, m, y;
};

struct employee{
	int empid;
	float salary;
	struct DateOfBirth dob;
};

void showDOB(struct DateOfBirth dob) {
	printf("\nDD/MM/YYYY\t: %d/%d/%d", dob.d, dob.m, dob.y);
}

void showEmployeeDetail(struct employee e) {
	printf("\nId\t\t:%d", e.empid);
	printf("\nSalary\t\t:%.2f", e.salary);
	showDOB(e.dob);
}
