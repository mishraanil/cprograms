#include <stdio.h>

int main(){
	
	int month;
	char flag;
	
	start: printf("Please enter the months in numbers: \n");
	scanf("%d",&month);
	
	switch(month){
		case 1:
			printf("January");
			break;
		case 2:
			printf("February");
			break;
		case 3:
			printf("March");
			break;
		case 4:
			printf("April");
			break;
		case 5:
			printf("May");
			break;
		case 6:
			printf("June");
			break;
		case 7:
		case 8:
		case 9:
			printf("Third Quarter");
			break;
		case 10:
		case 11:
		case 12:
			printf("Fourth Quarter");
			break;	
		default :// if none of the above case match then default block will excecute
		//it is not mandatory to give break for default block it's optional but for other cases it is mandatory 
			printf("Invalid character");
			goto start;
	}
	
	flagcont:printf("\nPlease enter y to continue or n to exit: ");
		scanf(" %c",&flag);
		
	switch(flag){
		case 'y':
		case 'Y':
			goto start;
			break;
		case 'n':
		case 'N':
			printf("Thank You Bye-Bye");
			break;
		default:
			printf("Invalid Cahracter");		
			goto flagcont;
	
	}		
	
}

