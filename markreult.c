#include <stdio.h> /*Include all files */


/*entry point of program */

int main(){
	/* varialble declaration */
	
	int english;
	int maths;
	int marathi;
	int Total;
	float Percent;
	
	/* varialble declaration ends */
	
	/* read marks from user starts */
	
	printf("Please enter the marks\n");
	printf("\nEnglish:");
	scanf("%d",&english);
	
	printf("\nMaths:");
	scanf("%d",&maths);
	
	printf("\nMarathi:");
	scanf("%d",&marathi);
	
	Total = english+maths+marathi;
	
	Percent= (Total/300.0)*100;
	
	printf("\nTotal: %d Percent Marks: %.2f",Total,Percent);
	
	printf("\n-------------------------");
	
	printf("\nSubject\tMark Obtained\t|");
	
	printf("\nEnglish\t\t%d\t|",english);
	printf("\nMaths\t\t%d\t|",maths);
	printf("\nMarathi\t\t%d\t|",marathi);
	printf("\n--------------------------------");
	printf("\nTotal %d Percent Obtained %.2f\t|",Total,Percent);
	printf("\n--------------------------------");
	
}
