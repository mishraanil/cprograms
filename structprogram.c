#include <stdio.h>
#include <string.h>

struct Student {
	
	char name[20];
	int age;
	int marks;
	float percent;
	
	
};

union employee {
	
	char name[200];
	int age;
	int salary;
	
};

int main(){
	
	int structuresize;
	int unionsize;
	struct Student s1;
	
	strcpy(s1.name, "Anil Mishra");
	s1.age = 33;
	s1.marks = 90;
	s1.percent = 90.1;
	
	printf("\n%s", s1.name);
	printf("\n%d",s1.age);
	printf("\n%d",s1.age);
	printf("\n%.2f %",s1.percent);
	
	
	struct Student s2;
	
	strcpy(s2.name,"Shailesh");
	s2.age = 25;
	s2.marks=99.99;
	s2.percent = 99.99;
	
	printf("\n");
	printf("\n%s",s2.name);
	printf("\n%d",s2.age);
	printf("\n%d",s2.marks);
	printf("\n%.2f%%",s2.percent);

	structuresize = sizeof(s1);
	printf("\nStructure s1 size is: %d",structuresize);
	printf("\nStructure s2 size is: %d",structuresize);
	
	
	union employee e1;
	
	strcpy(e1.name,"AM");
	e1.age = 33;
	e1.salary = 10000;
	
	
	printf("\n%s",e1.name);
	printf("\n%d",e1.age);
	printf("\n%d",e1.salary);
	
	unionsize = sizeof(e1);
	
	printf("\nUnio e1 size: %d",unionsize);
	
}



