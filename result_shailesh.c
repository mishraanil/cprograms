/* To calculate Total and Percentage of 5 subjects */

#include <stdio.h> // for printf()

int main(){ // entry point of program
	
	/* Variable Declaration [Starts] */
	int science;
	int maths;
	int english;
	int marathi;
	int history;
	int total;
	float percent;
	/* Variable Declaration [Ends] */
	
	/* Read marks from user [Starts] */
	printf("Please enter marks for following subjects\n");
	printf("\nscience:");
	scanf("%d", &science);
	
	printf("\nmaths:");
	scanf("%d", &maths);
	
	printf("\nenglish:");
	scanf("%d", &english);
	
	printf("\nmarathi:");
	scanf("%d", &marathi);
	
	printf("\nhistory:");
	scanf("%d", &history);
	/* Read marks from user [Ends] */
	
	/* Logic [Starts] */
	total 	= science + maths + english + marathi + history;
	percent	= (total/500.0) * 100;
	/* Logic [Ends] */
	
	/* Note: \t = tab */
	
	/* Print Output on Screen [Starts] */
	printf("-------------------------");
	printf("\n|Subject |\tMarks\t|");
	printf("\n-------------------------");
	printf("\n|Science\t%d\t|", science);
	printf("\n|Maths\t\t%d\t|", maths);
	printf("\n|English\t%d\t|", english);
	printf("\n|Marathi\t%d\t|", marathi);
	printf("\n|History\t%d\t|", history);
	printf("\n------------------------|");
	printf("\n|Total\t\t%d\t|", total);
	printf("\n-------------------------");
	printf("\nPercent\t\t%.2f\t|", percent);
	/* Print Output on Screen [Ends] */
}
