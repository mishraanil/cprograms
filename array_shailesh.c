/* Print array elements */
#include <stdio.h>

int main(){
	int arr[6]; // declaring ARRAY VARIABLE of SIZE 5 of type int
	// it will reserve 4 x 5 = 20 BYTES for array variable arr
	
	arr[0] = 1; // assigning VALUE 1 to array arr at index 0
	// by default INDEX number starts from 0
	// value of array at any index is called as "ELEMENT"
	
	arr[1] = 2;
	arr[2] = 3;
	arr[3] = 4;
	arr[4] = 55;
	
	printf("\nEnter value for last element of array\n");
	scanf("%d", &arr[5]);
	
	/* Accessing array variable */
	printf("%d", arr[3]);
	
	/* Accessing array variable using for loop */
	int i;
	
	int length = sizeof(arr) / sizeof(int); // logic to find length of array
	
	printf("\nAccessing array variable using for loop of length %d and array size %d", length, sizeof(arr));
	
	for(i = 0; i < length; i++){
		printf("\narr[%d] = %d", i, arr[i]);
	}
	
	getch();
}
