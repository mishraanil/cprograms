#include <stdio.h>

int main(){
	
	char name[20];
	char flag;
	int english;
	int maths;
	int hindi;
	int science;
	int marathi;
	int history;
	int total;
	float percent;
	
	start: printf("\nPlease enter the student name:");	
	scanf("%[^\n]s",&name);
	
	printf("\nStudent name: %s\n",name);
	
	printf("\nPlease enter the subject marks as prompt below\n");
	
	printf("English:");
	scanf("%d",&english);
	
	printf("Maths  :");
	scanf("%d",&maths);
	
	printf("Hindi  :");
	scanf("%d",&hindi);
	
	printf("Science:");
	scanf("%d",&science);
	
	printf("Marathi:");
	scanf("%d",&marathi);
	
	printf("History:");
	scanf("%d",&history);
	
	total = english+maths+hindi+science+marathi+history;
	percent=(total/600.0)*100;
	
	printf("\nTotal: %d Percent: %.2f",total,percent);
	
	printf("\nPlease enter Y for next student or enter N to exit: ");
	scanf(" %c",&flag);
	
	if (flag == 'Y'|| flag == 'y'){
		
		goto start;
		//goto proceed;
	}
	
	if (flag == 'N' || flag == 'n'){
		
		printf("Good Bye");
	}
	
	//proceed: printf("\nPlease enter the subject marks as prompt below\n");
	
}


