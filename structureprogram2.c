#include <stdio.h>
#include <string.h>

struct student {
	
	char name[20];
	int age;
	int marks;
		
};

void printStudent(struct student s){
	printf("\nName = %s",s.name);
	printf("\nAge = %d",s.age);
	printf("\nMarks = %d",s.marks);	
	printf("\n");
}

void getStudentData(struct student s) {
	printf("\nEnter Name:");
   	scanf(" %[^\n]s", &s.name);
   	
   	printf("\nEnter Age:");
   	scanf("%d",&s.age);
   	
   	printf("\nEnter Marks:");
   	scanf("%d",&s.marks);
}

int main(){
	
	struct student a[2];
	int i;
	
	strcpy(a[0].name, "Anil Mishra");
	a[0].age = 33;
	a[0].marks = 45;
	
	strcpy(a[1].name, "Akshit Mishra");
	a[1].age = 06;
	a[1].marks = 45;

   for(i=0; i<=1; i++){
   	/*
   	printf("\nEnter Name:");
   	scanf(" %[^\n]s", &a[i].name);
   	
   	printf("\nEnter Age:");
   	scanf("%d",&a[i].age);
   	
   	printf("\nEnter Marks:");
   	scanf("%d",&a[i].marks);
 	*/   	
 	getStudentData(a[i]);
   } 
	
	printStudent(a[0]);
	
	/*
	for(i = 0; i < 2; i++) {
		printf("\nName = %s",a[i].name);
		printf("\nAge = %d",a[i].age);
		printf("\nMarks = %d",a[i].marks);	
		printf("\n");
	}
	*/
}

