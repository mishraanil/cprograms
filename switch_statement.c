#include <stdio.h>

int main(){
	int day_number;
	
	start:printf("\nPlease enter day number:\n");
	scanf("%d", &day_number);
	
	switch(day_number){
		case 1: 
			printf("Monday");
			break;
		case 2: 
			printf("Tuesday");
			break;
		case 3: 
			printf("Wednesday");
			break;
		case 4: 
			printf("Thursday");
			break;
		default:
			printf("\nInvalid Number entered");
			goto start;
	}
	
	
	char flag;
	
	program_cont:printf("\nPlease enter y for continue and n for exit\n");
	scanf(" %c", &flag);
	
	switch(flag){
		case 'y':
		case 'Y':
				goto start;
				break;
		case 'n':
		case 'N':	
			printf("Good Bye");
			break;
		default:
			printf("\nPlease enter valid characher");
			goto program_cont;
	}
}
