#include <stdio.h>

int main() {
	char flag;
	//char : to store any letters or symbol or words
	//format specifier : %c
	
	printf("Please enter y to continue and n to exit:");
	scanf("%c", &flag);
	
	printf("\nSize of char data type: %d byte(s)", sizeof(char));
	
	printf("\nYou have entered %c", flag);
	
	char name[20] = "Anil Sharma"; //char name[] char array
	printf("\nName: %s", name); // char array : format specifier is %s
	
	char input_name[20];
	printf("\nPlease enter Students name:");
	scanf(" %[^\n]s", &input_name);
	printf("\nInput Name: %s", input_name) ;
	
	getch();
}
