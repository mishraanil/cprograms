#include <stdio.h>
#include "employee.h"

int main(){
	struct employee e1;
	
	e1.empid 	= 1234;
	e1.salary 	= 20000.50;
	e1.dob.d	= 20;
	e1.dob.m	= 10;
	e1.dob.y	= 1988;
	
	showEmployeeDetail(e1);
}
