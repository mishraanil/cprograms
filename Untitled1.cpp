#include <iostream>
using namespace std;

// main() is where program execution begins.

/*
multiline comment
*/
int main() {
	int x = 5;
	int y = 6;
	cout << "*************\n";
	cout <<"*   Hello   *\n";
	cout <<"*   world   *\n";
	cout <<"*************";//print Hello world
	cout << x + y;
	getchar();
	return 0;
}
